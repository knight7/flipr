const express = require("express");
const app = express();
const nodemailer = require("nodemailer");

//recognize the incoming Request Object as a JSON Object
app.use(express.json());

app.post("/", (req, res) => {
  const { email, email_content } = req.body;

  nodemailer.createTestAccount((err) => {
    if (err) {
      console.error("Failed to create a testing account. " + err.message);
      res.send({ sucess: "False", message: "Error message" });
      return process.exit(1);
    }

    console.log(" sending message...");

    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "o2mkko6huzxw5smi@ethereal.email",
        pass: "Sh1b7zyCBPbjTw6ttq",
      },
    });

    // Message object
    let message = {
      from: "Ritesh ritesh@example.com",
      to: email,
      subject: "Email sending Api",
      text: email_content,
      html: "<p><b>Email </b> SENT</p>",
    };

    transporter.sendMail(message, (err, info) => {
      if (err) {
        console.log("Error occurred. " + err.message);
        return process.exit(1);
      }

      console.log("Message sent: %s", info.messageId);
      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    });

    res.status(200).send({ sucess: "True", message: "Email Sent Succesfully" });
  });
});
// app.post("/", (req, res) => {
//   try {
//     const { email, email_content } = req.body;
//     // console.log(email);

//     // let testAccount = await nodemailer.createTestAccount();

//     // const transporter = nodemailer.createTransport({
//     //   host: "smtp.ethereal.email",
//     //   port: 587,
//     //   auth: {
//     //     user: "o2mkko6huzxw5smi@ethereal.email",
//     //     pass: "Sh1b7zyCBPbjTw6ttq",
//     //   },
//     // });
//     // let message = {
//     //   from: "ritesh@gmail.com",
//     //   to: "esh@gmail.com",
//     //   subject: "Email TEST",
//     //   text: email_content,
//     //   html: "<p><b>Hello</b> to myself!</p>",
//     // };
//     // let info = await transporter.sendMail(message);
//     // console.log("Message sent: %s", info.messageId);
//     // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//   } catch (err) {
//     console.log("");
//     res.send({ err: err });
//   }
// });

app.listen(3000, () => {
  console.log("Server is up on port 3000");
});
